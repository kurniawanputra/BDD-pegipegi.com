Behavior Driven Development for example pegipegi.com

Behavior Driven Development (BDD) is a popular software development methodology. 

What is Behavior Driven Development ?

outside-in, pull-based, multiple-stakeholder, multiple-scale, high-automation, agile methodology. It describes a cycle of interactions with well-defined outputs, resulting in the delivery of working, tested software that matters.

The idea of story BDD can be narrowed to:

describe features in a scenario with a formal text
use examples to make abstract things concrete
implement each step of a scenario for testing
write actual code implementing the feature
By writing every feature in User Story format that is automatically executable as a test we ensure that: business, developers, QAs and managers are in the same boat.

Each feature of a product should be born from a talk between : 

business (analysts, product owner)
developers
QA Engineer
which are known in BDD as "three amigos".

We can try to write such simple story:

"Feature: I search kereta
  I want go to holidays to city A
  As a tourist
  I want to price train

  Scenario: Search kereta api
    Given I open pegipegi.com
    And I click tab kereta api
    And I choose dari
    And I choose ke
    And I choose date pergi
    When I click search
    Then success list view price kereta api"

so for example tiket.com.
i create 3 scenario :
- i want buy ticket flight and view price list
- i want need holiday and i am booking hotel and view price list
- i want use transport via train and view price list

for more detail please see every feature in the project.

Thank you