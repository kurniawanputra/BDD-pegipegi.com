const { I } = inject();

Given(/I open pegipegi.com/, () => {
  I.amOnPage('https://www.pegipegi.com/');
  I.wait(10);
});

Given(/I click tab flight/, () => {
  I.click('.menuHeader-wrapper .menuHeader .nav.navbar-nav li:nth-child(2) a');
  I.wait(10);
});

Given(/I input flight dari/, () => {
  I.fillField('#asalKota1','');
  I.click('#listAsal1 .column:nth-child(2) .contentList:nth-child(3)');
  I.wait(10);
});

Given(/I input flight ke/, () => {
  I.fillField('#tujuanKota1','');
  I.click('#listtujuan1 .column:nth-child(3) .contentList:nth-child(6)');
  I.wait(10);
});

Given(/I choose date pergi/, () => {
  I.fillField('.formStart .inputForm .form-control','');
  I.click('.ui-datepicker-calendar tbody tr:nth-child(4) td:nth-child(7)');
  I.wait(10);
});

When(/I click search/, () => {
  I.click('#cari_tiket');
  I.wait(20);
});

Then(/success list view price flight/, () => {
  I.see('Filter');
});



