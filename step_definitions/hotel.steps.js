const { I } = inject();

Given(/I open pegipegi.com/, () => {
    I.amOnPage('https://www.pegipegi.com/');
    I.wait(10);
});

Given(/I click tab hotel/, () => {
    I.click('.menuHeader-wrapper .menuHeader .nav.navbar-nav li:nth-child(1) a');
    I.wait(10);
});

Given(/I choose hotel/, () => {
    I.fillField('#hotelNameKey', '');
    I.click('#formSearchHotel .wrapSearch .content .searchHoverHomeV2 ul li:nth-child(4)');
    I.wait(10);
});

Given(/I choose date pergi/, () => {
    I.fillField('#checkInDate', '');
    I.click('.ui-datepicker-calendar tbody tr:nth-child(4) td:nth-child(7)');
    I.wait(10);
});

When(/I click search/, () => {
    I.click('.twoColumn .right button');
    I.wait(20);
});

Then(/success list view price hotel/, () => {
    I.see('Filter Pencarian Hotel');
  });

