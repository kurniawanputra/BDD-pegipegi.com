const { I } = inject();

Given(/I open pegipegi.com/, () => {
    I.amOnPage('https://www.pegipegi.com/');
    I.wait(10);
});

Given(/I click tab kereta api/, () => {
    I.click('.menuHeader-wrapper .menuHeader .nav.navbar-nav li:nth-child(3) a');
    I.wait(10);
});

Given(/I choose dari/, () => {
    I.fillField('#kotaAsal', '');
    I.click('.lineForm .hoverInput .column:nth-child(3) .contentListWrap .contentList:nth-child(5)');
    I.wait(10);
});

Given(/I choose ke/, () => {
    I.fillField('#kotaTujuan', '');
    I.click('.lineStepForm .right .lineForm .hoverInput .column:nth-child(9) .contentListWrap .contentList:nth-child(1)');
    I.wait(10);
});

Given(/I choose date pergi/, () => {
    I.fillField('#tglPergi', '');
    I.click('.ui-datepicker-calendar tbody tr:nth-child(4) td:nth-child(7)');
    I.wait(10);
});

When(/I click search/, () => {
    I.click('#btnCari');
    I.wait(20);
});

Then(/success list view price kereta api/, () => {
    I.see('Filter');
  });






