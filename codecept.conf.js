exports.config = {
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'http://localhost',
      show: true,
      windowSize: "1380x960",
      chrome: {
        defaultViewport: {
          width: 1380,
          height: 960
        },
        disableScreenshots: false,
        args: [
          "--incognito",
          "--disable-infobars",
          "--windows-position=0,0",
          "--window-size=1380,960"
        ]
      }
    }
  },
  include: {
    I: './steps_file.js'
  },
  mocha: {},
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: [
      "./step_definitions/flight.steps.js",
      "./step_definitions/hotel.steps.js",
      "./step_definitions/kereta-api.steps.js"
    ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    }
  },
  tests: './*_test.js',
  name: 'BDD-pegipegi.com'
}